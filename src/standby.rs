use crate::parser::DateCell;
use chrono::prelude::*;
use std::fmt;

#[derive(Debug)]
pub struct Standby {
    pub start: NaiveDate,
    pub end: NaiveDate,
    pub name: String,
}

impl Standby {
    pub fn new(start: &DateCell, end: &DateCell) -> Option<Standby> {
        let now = Local::now();
        // ignore past standbys
        if end.date.date() <= now.date() {
            return None;
        }

        Some(Standby {
            start: start.date.naive_local().date(),
            end: end.date.naive_local().date(),
            name: "Standby".to_string(),
        })
    }
}

impl fmt::Display for Standby {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}  →  {}", self.start, self.end)
    }
}
