//! # Kalenteritunkki™

extern crate chrono;
extern crate google_calendar3 as calendar3;
extern crate hyper;
extern crate hyper_rustls;
#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate yup_oauth2;

use crate::calendar3::{Calendar, CalendarHub, CalendarListEntry, Event, EventDateTime};
use crate::oauth2::{Authenticator, DefaultAuthenticatorDelegate, DiskTokenStorage, GetToken};
use crate::standby::Standby;
use crate::workshift::Workshift;
use hyper::{net::HttpsConnector, Client};
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use yup_oauth2 as oauth2;

mod parser;
mod standby;
mod workshift;

type Hub =
    CalendarHub<Client, Authenticator<DefaultAuthenticatorDelegate, DiskTokenStorage, Client>>;

#[derive(Debug, Deserialize)]
pub struct Config {
    python_bin_linux: String,
    python_bin_windows: String,
    python_script: String,
    excel_path_linux: String,
    excel_path_windows: String,
    target_name: String,
}

fn main() {
    let mut file = File::open("Config.toml").expect("failed to open config");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("file read failed");
    let config: Config = toml::from_str(&contents).expect("failed to deserialize config");
    println!("{:#?}", config);

    let mut shifts = parser::parse(&config);

    let mut ready = false;
    println!("\n------------------------------------------------------------");
    print!("Ready for upload. ");
    while !ready {
        println!("Type 'go' to start, ctrl+c to quit");
        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();

        if input.trim() == "go" {
            ready = true;
        }
    }
    println!("\n------------------------------------------------------------");
    println!("Starting upload");

    let hub: Hub = get_hub();
    delete_calendar(&hub);
    let mut cal = create_calendar(&hub);
    // TODO: ad an automatic 1h? reminder to shifts and like 28h? to standbys
    push_shifts(&hub, &mut shifts, &mut cal);
}

fn create_calendar(hub: &Hub) -> Calendar {
    println!("Fetching calendar list");

    let mut cal: Calendar = Calendar::default();
    cal.summary = Some("Workshifts".to_string());
    let result = hub.calendars().insert(cal).doit();

    match result {
        Ok(t) => {
            println!("Calendar insert ok");
            t.1
        }
        Err(e) => {
            eprintln!("{:?}", e);
            panic!("Calendar insert failed");
        }
    }
}

fn delete_calendar(hub: &Hub) {
    println!("Deleting existing calendar");

    let calendar_list: Vec<CalendarListEntry> =
        hub.calendar_list().list().doit().unwrap().1.items.unwrap();

    for calendar in calendar_list {
        let summary = calendar.summary.unwrap();
        if summary == "Workshifts" {
            println!("Deleting existing calendar");
            let id = calendar.id.unwrap();
            let r = hub.calendars().delete(&id[..]).doit();
            match r {
                Ok(_t) => {
                    println!("Calendar deletion ok");
                }
                Err(e) => {
                    eprintln!("{:?}", e);
                    panic!("Calendar delete failed");
                }
            }
        }
    }
}

fn get_hub() -> Hub {
    let secret_path = Path::new("credentials.json");
    let secret =
        oauth2::read_application_secret(secret_path).expect("Couldn't read credentials file");
    let storage = DiskTokenStorage::new(&"token_storage.json".to_string()).unwrap();

    let client = Client::with_connector(HttpsConnector::new(hyper_rustls::TlsClient::new()));

    println!("Trying to auth");

    let mut auth = Authenticator::new(
        &secret,
        DefaultAuthenticatorDelegate,
        client,
        storage,
        Some(oauth2::FlowType::InstalledInteractive),
    );

    let _token = auth
        .token(&["https://www.googleapis.com/auth/calendar"])
        .unwrap();
    let client = Client::with_connector(HttpsConnector::new(hyper_rustls::TlsClient::new()));
    CalendarHub::new(client, auth)
}

fn push_shifts(hub: &Hub, shifts: &mut (Vec<Workshift>, Vec<Standby>), cal: &mut Calendar) {
    let id = cal.id.clone().unwrap();

    println!("Pushing workshifts...");
    for shift in &shifts.0 {
        let mut start = EventDateTime::default();
        start.date_time = Some(shift.start.to_rfc3339());
        let mut end = EventDateTime::default();
        end.date_time = Some(shift.end.to_rfc3339());
        let mut req = Event::default();
        req.summary = Some(shift.name.to_string());
        req.end = Some(end);
        req.start = Some(start);

        let result = hub.events().insert(req, &id[..]).doit();

        match result {
            Ok(_res) => println!("  insert ok - {}", shift),
            Err(e) => {
                delete_calendar(&hub);
                panic!("{:?}", e);
            }
        }
    }
    println!("Pushing standbys...");
    for standby in &shifts.1 {
        use chrono::Duration;
        use std::ops::Add;

        let mut start = EventDateTime::default();
        let mut end = EventDateTime::default();
        start.date = Some(standby.start.to_string());

        // standby ends at midnight so just add a day and dont try dicking around with 23:59:59
        let e = standby.end.add(Duration::days(1));
        end.date = Some(e.to_string());

        let mut req = Event::default();
        req.summary = Some(standby.name.to_string());
        req.start = Some(start);
        req.end = Some(end);

        let result = hub.events().insert(req, &id[..]).doit();

        match result {
            Ok(_res) => println!("  standby insert ok - {}", standby),
            Err(e) => {
                delete_calendar(&hub);
                panic!("{:?}", e);
            }
        }
    }
}
