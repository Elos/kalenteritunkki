use crate::parser::Cell;
use chrono::prelude::*;
use chrono::Duration;
use std::fmt;

#[derive(Debug)]
pub struct Workshift {
    pub start: DateTime<Local>,
    pub end: DateTime<Local>,
    pub duration: Duration,
    pub name: String,
}

impl fmt::Display for Workshift {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let dur = (self.duration.num_minutes() as f32) / 60.0;
        write!(f, "{}  →  {}   |  {}h", self.start, self.end, dur)
    }
}

impl Workshift {
    /// Returns a new workshift, ignoring everything before current date.
    pub fn new(cell: &Cell, date: DateTime<chrono::Local>) -> Option<Workshift> {
        use std::ops::{Add, Sub};

        let mut start = date;

        // TODO: make sure todays' shifts show up
        let now = Local::now();
        // ignore past shifts
        if start.date() <= now.date() {
            return None;
        }

        let mut end = date;
        let mut name: String = String::new();

        let duration = match cell.value.parse::<f32>() {
            Ok(d) => Duration::minutes((d * 60.0) as i64),
            Err(_e) => Duration::minutes(0),
        };

        let dur = (duration.num_minutes() as f32) / 60.0;

        if dur as i64 == 0 {
            return None;
        }

        match cell.color.as_ref() {
            "FFED7D31" | "FFA5A5A5" | "5" => {
                name = format!("Morning shift {}h", dur);

                start = start.add(Duration::hours(7));
                end = start.add(duration);
                if !is_weekend(start) {
                    end = end.add(Duration::minutes(30));
                }
            }
            "FF70AD47" => {
                name = format!("Night shift {}h", dur);

                if is_weekend(start) {
                    match duration.num_hours() {
                        12 => start = start.add(Duration::hours(19)),
                        _ => start = start.add(Duration::hours(23)),
                    };
                    end = start.add(duration);
                } else {
                    start = start.add(Duration::hours(21).add(Duration::minutes(30)));
                    end = start.add(duration);
                }
            }
            "FF5B9BD5" | "4" => {
                name = format!("Evening shift {}h", dur);

                if is_weekend(start) {
                    end = start.add(Duration::hours(23));
                } else {
                    end = start.add(Duration::hours(22));
                    start = end.sub(duration).sub(Duration::minutes(30));
                }
            }

            "7" => {
                name = format!("Day shift {}", dur);

                start = start.add(Duration::hours(9));
                end = start.add(duration).add(Duration::minutes(30));
            }
            _ => (),
        };

        Some(Workshift {
            start,
            end,
            duration,
            name,
        })
    }
}

fn is_weekend(date: DateTime<Local>) -> bool {
    (date.weekday() == Weekday::Sat) || (date.weekday() == Weekday::Sun)
}
