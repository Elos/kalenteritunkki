use super::Config;
use crate::standby::Standby;
use crate::workshift;
use chrono::prelude::*;
use std::process;
use std::process::Command;

#[derive(Debug)]
pub struct Item {
    pub date: String,
    pub hours: String,
    pub color: String,
}

#[derive(Debug, Clone)]
pub struct Cell {
    pub row: i32,
    pub col: i32,
    pub value: String,
    pub color: String,
}

#[derive(Debug, Clone)]
pub struct DateCell {
    pub row: i32,
    pub date: DateTime<Local>,
}

pub fn parse(config: &Config) -> (Vec<workshift::Workshift>, Vec<Standby>) {
    let mut cells: Vec<Cell> = Vec::new();
    let mut dates: Vec<DateCell> = Vec::new();

    parse_xlsx(config, &mut cells, &mut dates);

    let shifts = parse_shifts(&cells, &dates);
    let standbys = parse_standbys(&cells, &dates);

    (shifts, standbys)
}

fn parse_xlsx(config: &Config, cells: &mut Vec<Cell>, dates: &mut Vec<DateCell>) {
    let (path_python, path_excel) = get_paths(&config);

    let out = Command::new(&path_python)
        .arg(&config.python_script)
        .arg(&path_excel)
        .output()
        .expect("Error running python script");

    if !out.status.success() {
        let errorstr = String::from_utf8_lossy(&out.stderr);
        let code = out.status.code().unwrap();
        eprintln!("Status: {}, stderr:\n{}", out.status, errorstr);
        process::exit(code);
    }

    let out = String::from_utf8(out.stdout).unwrap();
    let out: Vec<&str> = out.split_terminator('\n').collect();
    let out: Vec<String> = out.into_iter().map(|s| s.trim().to_owned()).collect();

    for thing in &out {
        let thing: Vec<&str> = thing.split(';').collect();
        let row: i32 = thing[0].parse().expect("failed to parse int");
        let col: i32 = thing[1].parse().expect("failed to parse int");

        let cell = Cell {
            row,
            col,
            value: thing[2].to_string(),
            color: thing[3].to_string(),
        };
        cells.push(cell);
    }

    let date_cells: Vec<Cell> = cells.iter().cloned().filter(|x| x.col == 2).collect();
    let targets: Vec<&Cell> = cells
        .iter()
        .filter(|x| x.value.contains("Joonas"))
        .collect();

    let mut items: Vec<Cell> = Vec::new();

    for target in targets {
        let stop = cells
            .iter()
            .filter(|x| x.col == target.col)
            .filter(|x| x.row > target.row)
            .find(|x| x.value.starts_with("=SUM"))
            .expect("stop item not found");

        //println!("\nTARGET {:?}, STOP {:?}", target, stop);

        items.extend(
            cells
                .iter()
                .cloned()
                .filter(|x| x.col == target.col)
                .filter(|x| x.row > target.row)
                .filter(|x| x.row < stop.row),
        );
    }

    cells.clear();
    cells.append(&mut items);

    for cell in &date_cells {
        let date_cell = match NaiveDateTime::parse_from_str(&cell.value, "%Y-%m-%d %H:%M:%S") {
            Ok(d) => d,
            Err(_) => continue,
        };

        let date = Local.from_local_datetime(&date_cell).unwrap();

        let date_cell = DateCell {
            row: cell.row,
            date,
        };
        dates.push(date_cell);
    }
}

#[cfg(target_os = "linux")]
fn get_paths(config: &Config) -> (String, String) {
    (
        config.python_bin_linux.clone(),
        config.excel_path_linux.clone(),
    )
}

#[cfg(target_os = "windows")]
fn get_paths(config: &Config) -> (String, String) {
    (
        config.python_bin_windows.clone(),
        config.excel_path_windows.clone(),
    )
}

fn parse_shifts(cells: &[Cell], dates: &[DateCell]) -> Vec<workshift::Workshift> {
    println!("Parsing shifts...");
    let mut shifts: Vec<workshift::Workshift> = Vec::new();
    for cell in cells {
        let date_cell = dates.iter().find(|c| c.row == cell.row);

        let date_cell = match date_cell {
            Some(d) => d,
            None => continue,
        };

        if let Some(w) = workshift::Workshift::new(&cell, date_cell.date) {
            println!("  {}", w);
            shifts.push(w);
        }
    }
    if shifts.is_empty() {
        panic!("Empty shifts list");
    }
    shifts
}

fn parse_standbys(cells: &[Cell], dates: &[DateCell]) -> Vec<Standby> {
    // TODO: It's possible there's zero standby days so check and maybe make
    // the return value an Option or something.

    println!("Parsing standbys...");
    let mut standbys: Vec<Standby> = Vec::new();

    let mut standby_cells: Vec<&Cell> = cells.iter().filter(|x| x.color == "FFA5A5A5").collect();

    let mut standby_starts: Vec<&Cell> = Vec::new();
    let mut standby_ends: Vec<&Cell> = Vec::new();

    // First in the list is always a start.
    let first = standby_cells.remove(0);
    standby_starts.push(first);
    let mut prev = first.row;
    for cell in &standby_cells {
        if prev + 1 == cell.row {
            prev = cell.row;
            continue;
        } else {
            // Increment of more than one means current standby end and start of the next.
            standby_ends.push(standby_cells.iter().find(|x| x.row == prev).unwrap());
            standby_starts.push(standby_cells.iter().find(|x| x.row == cell.row).unwrap());
            prev = cell.row;
        }
    }
    // Last in the list is always an end.
    standby_ends.push(standby_cells.pop().unwrap());

    for (i, cell) in standby_starts.iter().enumerate() {
        let start = dates.iter().find(|x| x.row == cell.row).unwrap();
        let end = dates
            .iter()
            .find(|x| x.row == standby_ends.get(i).unwrap().row)
            .unwrap();
        if let Some(s) = Standby::new(start, end) {
            println!("  {}", s);
            standbys.push(s);
        }
    }
    standbys
}
