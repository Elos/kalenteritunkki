"""Parse row,col, value and color information from excel sheet."""
from openpyxl import load_workbook
from openpyxl.cell.read_only import EmptyCell
import sys

if __name__ == "__main__":
    sheet = load_workbook(sys.argv[1], read_only=True).active

    for row in sheet.iter_rows():
        for cell in row:
            if type(cell) is EmptyCell:
                continue
            try:
                color = cell.fill.start_color.index
            except AttributeError:
                color = 00000000
            print("{0};{1};{2};{3}".format(cell.row, cell.column, cell.value, color))
